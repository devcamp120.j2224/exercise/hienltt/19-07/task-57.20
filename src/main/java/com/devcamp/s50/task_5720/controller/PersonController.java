package com.devcamp.s50.task_5720.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task_5720.model.Address;
import com.devcamp.s50.task_5720.model.Professor;
import com.devcamp.s50.task_5720.model.Student;
import com.devcamp.s50.task_5720.model.Subject;

@RestController
public class PersonController {
    // Tạo API có tên listStudent trả ra danh sách sinh viên
    @CrossOrigin
    @GetMapping("/listStudent")
    public ArrayList<Student> getListStudent() {
        ArrayList<Student> students = new ArrayList<Student>();

        Subject subjectHoadaicuong = new Subject("Hoá Đại Cương", 1, new Professor(50, "male", "Tom Cruise", new Address())); 
        ArrayList<Subject> subject01s = new ArrayList<>();
        subject01s.add(subjectHoadaicuong);

        Student studentPeter = new Student(18, "male", "Peter", new Address(), 1, subject01s);
        Student studentMarry = new Student(19, "female", "Marry", new Address(), 2,
                    new ArrayList<Subject>() {
                    {
                        add(new Subject("Vật lý đại cương", 2, new Professor(50, "male", "Tom Cruise", new Address())));
                        add(new Subject("Hình học", 3, new Professor(55, "female", "Madona", new Address())));  
                    }
        });
        
        Student studentDiana = new Student(20, "female", "Diana", new Address(), 3,
                    new ArrayList<Subject>() {
                    {
                        add(new Subject("Anh Văn", 4, new Professor(50, "male", "Tom Cruise", new Address())));
                        add(new Subject("Giải tích", 5, new Professor(55, "female", "Madona", new Address())));  
                    }
        });

        students.add(studentPeter);
        students.add(studentMarry);
        students.add(studentDiana);
        return students;
    }
}
